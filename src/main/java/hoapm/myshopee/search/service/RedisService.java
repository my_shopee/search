package hoapm.myshopee.search.service;

import java.time.Duration;

public interface RedisService {
    void upsert(String key, Object value);

    void upsert(String key, Object value, Duration duration);

    Object get(String key);

    <T> T get(String key, Class<T> clazz);

    Boolean removeByKey(String key);

    void removeByKeyPrefix(String prefix);

    Boolean hashKey(String key);

}
