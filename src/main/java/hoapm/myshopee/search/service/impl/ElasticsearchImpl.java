package hoapm.myshopee.search.service.impl;

import hoapm.myshopee.search.dto.ProductDto;
import hoapm.myshopee.search.elasticsearch.index.Product;
import hoapm.myshopee.search.functional.Maybe;
import hoapm.myshopee.search.repository.CustomProductRepository;
import hoapm.myshopee.search.repository.ProductRepository;
import hoapm.myshopee.search.service.ElasticSearchService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ElasticsearchImpl implements ElasticSearchService {
    final private ProductRepository productRepository;
    final private CustomProductRepository customProductRepository;
    final private ModelMapper modelMapper;
    @Override
    public void createProduct(ProductDto productDto) {
        Product product = Maybe.of(productDto).map(p -> modelMapper.map(p, Product.class)).get();
        productRepository.save(product);
    }

    @Override
    public List<Product> searchProductByName(String name) {
        return customProductRepository.findProductByName(name);
    }
}
