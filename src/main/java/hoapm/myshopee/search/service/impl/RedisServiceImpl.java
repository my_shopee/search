package hoapm.myshopee.search.service.impl;

import hoapm.myshopee.search.functional.Maybe;
import hoapm.myshopee.search.service.RedisService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Objects;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class RedisServiceImpl implements RedisService {
    final private RedisTemplate<String, Object> redisTemplate;

    @Override
    public void upsert(String key, Object value) {
        this.redisTemplate.opsForValue().set(key, value);
    }

    @Override
    public void upsert(String key, Object value, Duration duration) {
        this.redisTemplate.opsForValue().set(key, value, duration);
    }

    @Override
    public Object get(String key) {
        return this.redisTemplate.opsForValue().get(key);
    }

    @Override
    public <T> T get(String key, Class<T> clazz) {
        return Maybe.of(key).map(this::get).castValueTo(clazz).orElseNull();
    }

    @Override
    public Boolean removeByKey(String key) {
        return this.redisTemplate.delete(key);
    }

    @Override
    public void removeByKeyPrefix(String prefix) {
        Set<String> keys = this.redisTemplate.keys(prefix + "*");
        if (Objects.nonNull(keys)) {
            for (String key : keys
            ) {
                Maybe.of(key).map(redisTemplate::delete);
            }
        }
    }

    @Override
    public Boolean hashKey(String key) {
        return Maybe.of(key).map(redisTemplate::hasKey).orElse(false);
    }
}
