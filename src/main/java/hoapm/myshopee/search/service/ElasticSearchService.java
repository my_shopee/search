package hoapm.myshopee.search.service;

import hoapm.myshopee.search.dto.ProductDto;
import hoapm.myshopee.search.elasticsearch.index.Product;

import java.util.List;

public interface ElasticSearchService {
    void createProduct(ProductDto productDto);

    List<Product> searchProductByName(String name);
}
