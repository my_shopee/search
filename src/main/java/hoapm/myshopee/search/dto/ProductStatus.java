package hoapm.myshopee.search.dto;

public enum ProductStatus {
    AVAILABLE("AVAILABLE"),
    OUT_OF_STOCK("OUT_OF_STOCK");
    private String name;
    private ProductStatus(String name) {
        this.name = name;
    }
}
