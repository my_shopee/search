package hoapm.myshopee.search.contructor;

import hoapm.myshopee.search.dto.ProductDto;
import hoapm.myshopee.search.service.ElasticSearchService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequiredArgsConstructor
@RequestMapping("/elasticsearch")
public class ElasticsearchController {
    final private ElasticSearchService elasticSearchService;

    @GetMapping()
    public ResponseEntity<?> findProductsByName(@RequestParam(name = "name") String name) {
        return ResponseEntity.ok(elasticSearchService.searchProductByName(name));
    }

    @PostMapping()
    public ResponseEntity<?> createProduct(@RequestBody ProductDto productDto) {
        elasticSearchService.createProduct(productDto);
        return ResponseEntity.ok(new HashMap<>());
    }
}
