package hoapm.myshopee.search.repository;

import hoapm.myshopee.search.elasticsearch.index.Product;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface ProductRepository extends ElasticsearchRepository<Product, String> {
    List<Product> findProductsByName(String name);
    List<Product> findProductsByCategory(String category);

}
