package hoapm.myshopee.search.repository.impl;

import hoapm.myshopee.search.constant.Constant;
import hoapm.myshopee.search.elasticsearch.index.Product;
import hoapm.myshopee.search.repository.CustomProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.IndexedObjectInformation;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor
@Slf4j
public class CustomProductRepoImpl implements CustomProductRepository {
    private final ElasticsearchOperations elasticsearchOperations;

    @Override
    public List<IndexedObjectInformation> insertBulkProductIndex(List<Product> products) {
        List<IndexQuery> queries = products.stream()
                .map(product -> new IndexQueryBuilder()
                        .withId(product.getId())
                        .withObject(product)
                        .build())
                .toList();
        return elasticsearchOperations.bulkIndex(queries, IndexCoordinates.of(Constant.Index.PRODUCT_INDEX));
    }

    @Override
    public String insertProductIndex(Product product) {
        IndexQuery indexQuery = new IndexQueryBuilder()
                .withId(product.getId())
                .withObject(product)
                .build();
        return elasticsearchOperations.index(indexQuery, IndexCoordinates.of(Constant.Index.PRODUCT_INDEX));
    }

    @Override
    public List<Product> findProductByName(String name) {
        Criteria criteria = new Criteria("name").is(name);
        Query searchQuery = new CriteriaQuery(criteria);
        SearchHits<Product> productSearchHits = elasticsearchOperations.search(searchQuery, Product.class, IndexCoordinates.of(Constant.Index.PRODUCT_INDEX));
        List<Product> result = new ArrayList<>();
        productSearchHits.forEach(productSearchHit -> {
            result.add(productSearchHit.getContent());
            log.info("Score on search product: {}", productSearchHit.getScore());
        });
        return result;
    }
}
