package hoapm.myshopee.search.repository;

import hoapm.myshopee.search.elasticsearch.index.Product;
import org.springframework.data.elasticsearch.core.IndexedObjectInformation;

import java.util.List;

public interface CustomProductRepository {
    List<IndexedObjectInformation> insertBulkProductIndex(final List<Product> products);

    String insertProductIndex(Product product);

    List<Product> findProductByName(String name);
}
