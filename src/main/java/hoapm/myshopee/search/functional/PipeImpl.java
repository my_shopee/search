package hoapm.myshopee.search.functional;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Function;


@Getter
@Setter
@Slf4j
public class PipeImpl<I> implements Pipe<I> {

    private I input;
    private Pipe<?> prev;
    private Pipe<?> next;

    private int index;

    private PipeImpl(I input) {
        this.input = input;
    }

    public static <K> Pipe<K> of(K input) {
        return new PipeImpl<>(input)    ;
    }

    @Override
    public <O> Pipe<O> exec(Function<I, O> func) {
        if (input instanceof Exception) {
            return (Pipe<O>) this;
        }
        try {
            O out = func.apply(input);
            this.next = PipeImpl.of(out);
        } catch (Exception e) {
            this.next = PipeImpl.of(e);
            log.error(">>> error when processing {} at step {}", input, index + 1, e);
        } finally {
            this.next.setPrev(this);
            this.next.setIndex(index + 1);
        }
        return (Pipe<O>) next;
    }


    @Override
    public <O> void setPrev(Pipe<O> input) {
        this.prev = input;
    }


    public void setIndex(int index) {
        this.index = index;
    }
}
