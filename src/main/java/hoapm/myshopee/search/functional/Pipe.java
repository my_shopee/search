package hoapm.myshopee.search.functional;

import java.util.function.Function;

public interface Pipe<I> {
    <O> Pipe<O> exec(Function<I,O> func);

    <O> void setPrev(Pipe<O> input);
    void setIndex(int index);
    int getIndex();
}
