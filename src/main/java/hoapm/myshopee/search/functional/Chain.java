package hoapm.myshopee.search.functional;

import java.util.function.Function;

public class Chain<I, O> {
    private final Function<I, O> executor;

    public Chain(Function<I, O> executor) {
        this.executor = executor;
    }

    public <K> Chain<I, K> then(Function<O, K> afterFunc) {
        return new Chain<>(input -> afterFunc.apply(executor.apply(input)));
    }

    public O execute(I input) {
        return executor.apply(input);
    }
}
