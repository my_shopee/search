package hoapm.myshopee.search.functional;

public interface ThrowingFunction<T, R> {
    R apply(T t) throws Exception;
}
