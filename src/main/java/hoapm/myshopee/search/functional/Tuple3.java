package hoapm.myshopee.search.functional;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class Tuple3<T, V, U> {
    T t1;
    V t2;
    U t3;

    public Tuple3(T t1, V t2, U t3) {
        this.t1 = t1;
        this.t2 = t2;
        this.t3 = t3;
    }
}
