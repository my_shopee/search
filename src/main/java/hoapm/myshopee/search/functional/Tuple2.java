package hoapm.myshopee.search.functional;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class Tuple2<K, V> {
    K t1;
    V t2;

    public Tuple2(K t1, V t2) {
        this.t1 = t1;
        this.t2 = t2;
    }
}
