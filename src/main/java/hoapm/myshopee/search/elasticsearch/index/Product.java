package hoapm.myshopee.search.elasticsearch.index;

import hoapm.myshopee.search.dto.ProductStatus;
import jakarta.persistence.Id;
import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "product_index")
@Data
public class Product {
    @Id
    private String id;
    @Field(type = FieldType.Text, name = "name")
    private String name;
    @Field(type = FieldType.Text, name = "category")
    private String category;
    @Field(type = FieldType.Double, name = "price")
    private Double price;
    @Field(type = FieldType.Text, name = "status")
    private ProductStatus status;

    @Field(type = FieldType.Long, name = "quantity")
    private Long quantity;
}
